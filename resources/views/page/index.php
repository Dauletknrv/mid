<!html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<title>Chocolife</title>
	<meta name="theme-color" content="#e31e24">
	<meta property="og:image"
		  content="static/img/frontend/b-logo--big.png" />
	<link rel="shortcut icon" href="static/img/frontend/favicon.ico">
		
	<link type="text/css" href="min/frontend_cssf9a1.css?v=60" rel="stylesheet" />		
	<script type="text/javascript" src="min/frontend_jsbcfe.js?v=22"></script>	<script type="text/javascript" src="min/mainpage_jsff23.js?v=166"></script>	
</head>
<body class="main_body ">
	
<div id="modal-overlay"></div>
<div id="dropdown_overlay"></div>

<div class="b-holding__wrapper">
    <div class="b-holding__menu">
                		<ul class="b-project__list">
            <li class="e-project__item e-project__item--chocolife">
                <a href="index.html"
                   class="e-project__link"
                   title="chocolife.me">
                </a> </li>
            <li class="e-project__item e-project__item--chocomart">
                <a href="#"
                   target="_blank"
                   rel="nofollow"
                   class="e-project__link"
                   title="chocomart.kz">
                </a></li>
            <li class="e-project__item e-project__item--chocotravel">
                <a href="#"
                   target="_blank"
                   class="e-project__link"
                   title="chocotravel.com">
                </a></li>
            <li class="e-project__item e-project__item--lensmark">
                <a href="#"
                   target="_blank"
                   rel="nofollow"
                   class="e-project__link"
                   title="lensmark.kz">
                </a></li>
            <li class="e-project__item e-project__item--chocofood">
                <a href="#"
                   target="_blank"
                   rel="nofollow"
                   class="e-project__link"
                   title="chocofood.kz">
                </a></li>
            <li class="e-project__item e-project__item--idoctor">
                <a href="#"
                   target="_blank"
                   rel="nofollow"
                   class="e-project__link"
                   title="idoctor.kz">
                </a></li>
        </ul>                                    <div class="b-user__menu">
                    <div class="b-menu__account">
						<span class="e-account__menu__registration"><a
                                    href="#"
                                    id="reg_btn"
                                    title="Регистрация"
                                 >Регистрация</a></span>
                        <span class="e-account__trigger float-left">
                            <span class="e-account__menu__signin"><a
                                        href="#"
                                        id="login_btn"
                                        title="Вход"
                                       >Вход</a></span>
                        </span>
                    </div>
                    <div class="b-menu__cart">
                        <a href="cart/view/index.html">
                            <div class="e-cart__icon">
                                <div class="e-cart__counter">
                                    0                                </div>
                            </div>
                        </a>
                    </div>
                </div></div>
</div>
<div class="b-recharge__root"></div>

<header class="b-header"
        id="js-b-header">
    <div class="b-header_inner">
    <div class="header_left">
        <div class="e-search__quicklinks">
            <div class="b-city__change_wrapper">
                <div class="b-city__change">
                    <a data-href="#" class="e-city__wrapper js-e-current__city--main">
				            <span class="e-current__city">
					            Алматы	</span>
                    </a>
                    <span class="e-open__city__list e-icon js-e-current__city--main"></span>
                    <div class="clear"></div>
                </div>
            </div>
                            <span hashstring="header_some_answer" hashtype="content">&nbsp;</span>
                    </div>
    <div class="b-logo__city__wrapper">
        <div class="b-logo">
            <a class="e-logo--big_w" href="index.html"><span class="e-logo e-logo--big"></span></a>
            <p class="e-logo__text">Главное, чтобы Вы<br>
                были счастливы!</p>
        </div>
    </div>
            <div class="b-warranty__search__wrapper"
         id="js-b-warranty__search__wrapper">
        <div class="b-search">
            <form action="#"
                  id="search"
                  class="b-search__form b-search--main">
                <input type="search"
                       placeholder="Найти среди 1000 акций"
                       class="e-search__input e-search__input--main"
                       autocomplete="off"
                       name="query"
                       id="js-e-search__input"
                       value=""
                >
                <div class="clear_search_input"></div>
                <button type="submit" class="e-search__submit" disabled="disabled"></button>
                <div class="search_shadow"></div>
            </form>
        </div>
                    <div class="b-warranty__header_phone" style="display: none;">
                <p class="b-warranty__header_phone_txt1">+7 (727)&nbsp;346-85-88</p>
                <div class="b-warranty__header_phone_txt2">Остались вопросы? Звоните</div>
            </div>
            </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <div id="warranty__quality" style="display: none;">
        <div class="b-warranty__img b-warranty__img--quality"></div>
        <div class="b-warranty_block">
            <span class="e-warranty--header">Гарантия качества</span>
			<span class="e-warranty--text">
				Качество акции - основной приоритет компании.<br>
				Центр контроля качества работает напрямую<br>
				с представителями каждого заведения и оперативно решает любые проблемы.</span>
        </div>
    </div>
    <div id="warranty__back" style="display: none;">
        <div class="b-warranty__img b-warranty__img--back"></div>

        <div class="b-warranty_block">
            <span class="e-warranty--header">Лояльность</span>
			<span class="e-warranty--text">
				Если Вам не понравилось заведение или обслуживание по акции -
				<a href="feedback/index.html" class="e-some__answer"> просто дайте нам знать </a>
				и мы обязательно Вам поможем.
			</span>
        </div>
    </div>
    <div id="warranty__client" style="display: none;">
        <div class="b-warranty__img b-warranty__img--client"></div>
        <div class="b-warranty_block">
            <span class="e-warranty--header">СЗП</span>
			<span class="e-warranty--text">
				Наша Служба заботы о пользователях работает круглосуточно и без выходных. Если у Вас возникнут вопросы или предложения, то звоните на номер +7(727)321-85-88 или пишите в Форму обратной связи.
			</span>
        </div>
    </div>
    </div>
</header>
<!-- 	Block: City_links -->
<div class="b-city_links__wrapper">
	<div class="city_double" onclick="close_city_links()">
		<p class="city_double_txt">Алматы</p>
		<span class="city_double_triangle"></span>
	</div>
</div>
<!-- 	End Block: City_links -->
<div class="b-banner b-banner__height">
    <span class="b-banner__closer">×</span>
    <div class="b-banner__revive {zoneID: 40, source: 'b-banner__revive_zone40'}" id="b-banner__revive_zone40"></div>
</div>
	<div class="e-bn-blocked-plug">
		<img src="static/img/frontend/header/adblock_plug3860.jpg?v=1" alt="" usemap="#map-adblock" />
		<map name="map-adblock">
			<area shape="rect" coords="940, 0, 979, 43" onclick="closeAdblockMessage(); return false;" />
		</map>
	</div>

<div id="adsense_inline"></div>
<a id="scrollToAnchor"></a>


<div id="b-deals__menunav__category__place">
<div id="navigator">
	<div class="b-category__navigator_w">
	<div class="b-category__navigator_w2" data-categoryisshow="categoryisshow">
	<ul class="b-category__navigator">
				<li class="e-category__navigator e-category__navigator--selected">
			<a class="main_mirror" href="index.html" data-url="/" cat_id="1"
			   parent_id="0" data-emarsysTitle="Все">
				Все
			</a>
		</li>

				<li class="e-category__navigator " id="menu_category_2">
						<div id="dd" class="wrapper-dropdown-5" tabindex="1">
							<a 								class="main_mirror" href="novie/index.html"
								data-url="/novie/" cat_id="2"
								parent_id="1" data-emarsysTitle="Все>Новые" >
								Новые							</a>
							<ul class="dropdown">
															</ul>
						</div>
				</li>

			

				<li class="e-category__navigator " id="menu_category_173">
						<div id="dd" class="wrapper-dropdown-5" tabindex="1">
							<a 								class="main_mirror" href="khity-prodazh/index.html"
								data-url="/khity-prodazh/" cat_id="173"
								parent_id="1" data-emarsysTitle="Все>Хиты продаж" >
								Хиты продаж							</a>
							<ul class="dropdown">
															</ul>
						</div>
				</li>

			

				<li class="e-category__navigator " id="menu_category_5">
						<div id="dd" class="wrapper-dropdown-5" tabindex="1">
							<a 								class="main_mirror" href="razvlecheniya-i-otdykh/index.html"
								data-url="/razvlecheniya-i-otdykh/" cat_id="5"
								parent_id="1" data-emarsysTitle="Все>Развлечения и отдых" >
								Развлечения и отдых</a>	
						</div>
				</li>

			

				<li class="e-category__navigator " id="menu_category_3">
						<div id="dd" class="wrapper-dropdown-5" tabindex="1">
							<a 								class="main_mirror" href="krasota-i-zdorove/index.html"
								data-url="/krasota-i-zdorove/" cat_id="3"
								parent_id="1" data-emarsysTitle="Все>Красота и здоровье" >
								Красота и здоровье							</a>
							
						</div>
				</li>

				<li class="e-category__navigator " id="menu_category_36">
						<div id="dd" class="wrapper-dropdown-5" tabindex="1">
							<a 								class="main_mirror" href="sport/index.html"
								data-url="/sport/" cat_id="36"
								parent_id="1" data-emarsysTitle="Все>Спорт" >
								Спорт							</a>
							
						</div>
				</li>

				<li class="e-category__navigator " id="menu_category_7">
						<div id="dd" class="wrapper-dropdown-5" tabindex="1">
							<a 								class="main_mirror" href="tovary/index.html"
								data-url="/tovary/" cat_id="7"
								parent_id="1" data-emarsysTitle="Все>Товары" >
								Товары							</a>
							
						</div>
				</li>

						</ul>
	</div>
	</div>
</div>
</div>
	<div id="b-deals__menunav__category__place2">
	<div class="b-subcategory__navigator_w">
	<ul class="b-subcategory__navigator ">
								<li class="e-subcategory__navigator  ">
							<a  href="aktivniy-otdykh/index.html" data-url="/aktivniy-otdykh/" cat_id="18" parent_id="5" data-emarsysTitle="Все>Развлечения и отдых>Активный отдых">
								<span >Активный отдых</span>
							</a>
						</li>			<li class="e-subcategory__navigator  ">
						
	</ul>
	</div>
</div>

<div class="b-deals_main_sort-wrapper">
	<div class="b-deals_main_sort js-b-deals_main_sort ">
		<table>
			<tr>
				<td class="e-sort_title">Сортировать: </td>
				<td class="js-mainpage-deal-sorting">
					<input id="e-sort-order_type_popular" type="radio" name="order_type" value="popular" data-t="1" data-c="1" /> <label
						for="e-sort-order_type_popular"><span></span>популярные</label>
				</td>
				<td class="js-mainpage-deal-sorting">
					<input id="e-sort-order_type_price" type="radio" name="order_type" value="price" data-t="1" data-c="1" /> <label for="e-sort-order_type_price"><span></span>цена</label>
					<div class="b-main_sort_arrows b-main_sort-for-price">
						
					</div>
				</td>
				<td class="js-mainpage-deal-sorting">
					<input id="e-sort-order_type_discount" type="radio" name="order_type" value="discount" data-t="1" data-c="1" /> <label for="e-sort-order_type_discount"><span></span>скидка</label>
					<div class="b-main_sort_arrows b-main_sort-for-discount">
						
					</div>
				</td>
				<td class="js-mainpage-deal-sorting">
					<input id="e-sort-order_type_new" type="radio" name="order_type" value="new" data-t="1" data-c="1" /> <label for="e-sort-order_type_new"><span></span>новые</label>
				</td>
				<td class="js-mainpage-deal-sorting">
					<input id="e-sort-order_type_rating" type="radio" name="order_type" value="rating" data-t="1" data-c="1" /> <label for="e-sort-order_type_rating"><span></span>рейтинг</label>
					<div class="b-main_sort_arrows b-main_sort-for-rating">
					
					</div>
				</td>
							</tr>
		</table>
	</div>
</div>

><div class="js-e-loader_new_deals"></div><div id="b-dealslist_mainpage" class="b-deals__wrapper">
	<ul class="b-deals__list">
		<li class="b-deal b-deals__standard_plate b-deals__plate_width315" data-categories="[ 1 , 18 , 5 , 161 ]"   data-coordinates='[[77.058303,43.158238]]'>
		<span class="e-deal__protection"></span>
<div class="e-deal__imgs">
		    <a href="medeu2/index.html"  >
						<img
				width="315"
				height="315"
				src="../chocolife.me/img.s3.chocolife.me/static/upload/images/deal/for_plate/39564d30c.jpg?v=1515412461"
				class="e-plate__img"
				alt="Катайтесь в свое удовольствие на живописном высокогорном катке Медеу в будни и выходные со скидкой 30% до конца сезона!"
				/>
			</a>
					<a href="medeu2/index.html" class="e-link--deal plate_link">
				Подробнее
			</a>
	</div>
<div class="b-deals__standard_plate__counter" style="left: 21px;top: 282px;color: #ffffff;">

			Купили: 21089	
</div></li><li class="b-deal b-deals__standard_plate b-deals__plate_width315" data-categories="[ 1 , 58 , 3 , 76 , 5 , 96 , 5 , 110 , 173 ]"   data-coordinates='[]'>
	<div class="e-deal__imgs">
		    <a href="38525-tau-spa-center/index.html"  >
						<img
				width="315"
				height="315"
				src="../chocolife.me/img.s3.chocolife.me/static/upload/images/deal/for_plate/385251e67.jpg?v=1514447453"
				class="e-plate__img"
				alt="Еще дешевле! Посетите уникальный Тау SPA-center в будние и выходные дни со скидкой 61%!"
				/>
			</a>
					<a href="38525-tau-spa-center/index.html" class="e-link--deal plate_link">
				Подробнее
			</a>
	</div>
<div class="b-deals__standard_plate__counter" style="left: 20px;top: 281px;color: #ffffff;">

			Купили: 7527	
</div></li><li class="b-deal " data-categories="[ 1 , 82 , 8 , 110 , 173 ]"  data-coordinates='[]' data-id="36354">
	
	<span class="e-deal__discount">-46%</span>


<a href="36354-otel-tau-house/index.html" class="b-main_page__link" title="Проживание в лучших номерах на 1 сутки в отеле Тау House со скидкой до 46%!">
	<div class="e-deal__imgs">
		
			
					<img width="310"
					             height="240"
					             alt="Проживание в лучших номерах на 1 сутки в отеле Тау House со скидкой до 46%!"
					             src="../chocolife.me/static.chocolife.me/static/upload/images/deal/for_deal_page/37000/36354/310x240/1_201705310351614962219363044.jpg"
					             class="e-deal__img"
					             >
								</div>



	<div class="b-deal__bought">
		<span class="e-deal__link">Купили:
	        <span class="e-deal__count" data-id="36354"> 826 </span>
	    </span>
	</div>
	<div class="b-deal__info">
														       <p class="deals_bold">от&nbsp;</p>
							<span class="e-deal__price e-deal__price--old ">
				36 000			</span>
								
									<!-- Рейтинг акции -->
					<div class=" otzivy_deal_raiting_element_rating" style="height: 26px">
						<div class="hart1_inbox_rating">
		
							<div class="deal_rating_rating"
								 style="width:102.3px">
							</div>
						</div>
					</div>
						<span class="e-deal__price ">
						20 000&nbsp;тг.		</span>
						<span class="e-link--deal e-link--deal-none-border">Подробнее</span>
	</div>
	<div class="e-deal__text-wrapper">
		<div class="e-deal__text-inner">
            <div class="e-deal__title e-deal__title-no-margin">
                Отель Тау House            </div>
            <p class="e-deal__text" >
                Проживание в лучших номерах на 1 сутки            </p>
        </div>
	</div>
</a>
</li>
<li class="b-deal b-deals__standard_plate b-deals__plate_width315" data-categories="[ 1 , 96 , 5 , 110 , 173 ]"   data-coordinates='[]'>
	<div class="e-deal__imgs">
		    <a href="hawaii2/index.html"  >
						<img
				width="315"
				height="315"
				src="../chocolife.me/img.s3.chocolife.me/static/upload/images/deal/for_plate/36779febe.jpg?v=1513356169"
				class="e-plate__img"
				alt="Отдохните с семьей и друзьями в тематическом аквапарке Hawaii! Скидка 48% на безлимитный билет в будние и выходные дни!"
				/>
			</a>
					<a href="hawaii2/index.html" class="e-link--deal plate_link">
				Подробнее
			</a>
	</div>
<div class="b-deals__standard_plate__counter" style="left: 21px;top: 280px;color: #ffffff;">

			Купили: 426	
</div></li><li class="b-deal b-deals__standard_plate b-deals__plate_width315" data-categories="[ 1 , 82 , 8 , 110 , 173 ]"   data-coordinates='[]'>
	<div class="e-deal__imgs">
		    <a href="#"  >
						<img
				width="315"
				height="315"
				src="../chocolife.me/img.s3.chocolife.me/static/upload/images/deal/for_plate/392095a30.jpg?v=1514535832"
				class="e-plate__img"
				alt="Проживание в будние и выходные дни в горнолыжном курорте Табаган! Скидка до 55%"
				/>
			</a>
					<a href="39209-tabagan/index.html" class="e-link--deal plate_link">
				Подробнее
			</a>
	</div>
<div class="b-deals__standard_plate__counter" style="left: 23px;top: 279px;color: #ffffff;">

			Купили: 432	
</div></li><li class="b-deal b-deals__standard_plate b-deals__plate_width315" data-categories="[ 1 , 18 , 5 , 110 ]"   data-coordinates='[[77.095825,43.211773]]'>
	<div class="e-deal__imgs">
		    <a href="39540-srk-tabagan/index.html"  >
						<img
				width="315"
				height="315"
				src="../chocolife.me/img.s3.chocolife.me/static/upload/images/deal/for_plate/39540cc26.jpg?v=1514292623"
				class="e-plate__img"
				alt="Экстремально круто! Абсолютно новый снежный склон ждет вас! Катания на лыжах и сноуборде для взрослых и детей со скидкой до 30% в СРК Табаган!"
				/>
			</a>
					<a href="39540-srk-tabagan/index.html" class="e-link--deal plate_link">
				Подробнее
			</a>
	</div>
<div class="b-deals__standard_plate__counter" style="left: 14px;top: 280px;color: #005b92;">

			Купили: 2071	
</div></li><li class="b-deal b-deals__standard_plate b-deals__plate_width315" data-categories="[ 1 , 19 , 5 , 110 , 161 , 173 , 179 ]"   data-coordinates='[]'>
		<span class="e-deal__protection"></span>
<div class="e-deal__imgs">
		    <a href="36368-delfinariy-nemo/index.html"  >
						<img
				width="315"
				height="315"
				src="../chocolife.me/img.s3.chocolife.me/static/upload/images/deal/for_plate/36368d4dd.jpg?v=1515480825"
				class="e-plate__img"
				alt="Шоу-программа «Дари Любовь»! Весёлое шоу с артистами и морскими обитателями в алматинском дельфинарии Nemo со скидкой 20%!"
				/>
			</a>
					<a href="36368-delfinariy-nemo/index.html" class="e-link--deal plate_link">
				Подробнее
			</a>
	</div>
<div class="b-deals__standard_plate__counter" style="left: 24px;top: 278px;color: #ffffff;">

			Купили: 559	
</div></li><li class="b-deal " data-categories="[ 1 , 19 , 5 , 75 , 5 , 110 ]"  data-coordinates='[]' data-id="36976">
	
	<span class="e-deal__discount">-50%</span>


<a href="36976-funky-town-v-molle-aport/index.html" class="b-main_page__link" title="Аттракционы, игровые автоматы в лучшем развлекательном парке Казахстана Funky Town в крупнейшем молле страны Апорт! Скидка до 55%">
	<div class="e-deal__imgs">
		
					<img width="310"
					             height="240"
					             alt="Аттракционы, игровые автоматы в лучшем развлекательном парке Казахстана Funky Town в крупнейшем молле страны Апорт! Скидка до 55%"
					             src="../chocolife.me/static.chocolife.me/static/upload/images/deal/for_deal_page/37000/36976/310x240/10_201703281130814906782889629.jpg"
					             class="e-deal__img"
					             >
								</div>

	<div class="b-deal__bought">
		<span class="e-deal__link">Купили:
	        <span class="e-deal__count" data-id="36976"> 622 </span>
	    </span>
	</div>
	<div class="b-deal__info">
														       <p class="deals_bold">от&nbsp;</p>
							<span class="e-deal__price e-deal__price--old ">
				10 000			</span>
								
									<!-- Рейтинг акции -->
					<div class=" otzivy_deal_raiting_element_rating" style="height: 26px">
						<div class="hart1_inbox_rating">
							<div class="hart_ico_rating"></div>
							<div class="deal_rating_rating"
								 style="width:106.3px">
							</div>
						</div>
					</div>
						<span class="e-deal__price ">
						5 000&nbsp;тг.		</span>
						<span class="e-link--deal e-link--deal-none-border">Подробнее</span>
	</div>
	<div class="e-deal__text-wrapper">
		<div class="e-deal__text-inner">
            <div class="e-deal__title e-deal__title-no-margin">
                Funky Town в молле Апорт            </div>
            <p class="e-deal__text" >
                Аттракционы, игровые автоматы            </p>
        </div>
	</div>
</a>
</li>
<li class="b-deal " data-categories="[ 1 , 99 , 5 , 160 ]"  data-coordinates='[[76.915103,43.237207]]' data-id="39421">
	
	<span class="e-deal__discount">-30%</span>


<a href="39421-cinema-towers-3d/index.html" class="b-main_page__link" title="Мы работаем! Идеальное сочетание от Cinema Towers 3D: 2 билета на любой сеанс + 2 маленьких попкорна + 2 колы и скидка 30%!">
	<div class="e-deal__imgs">
		
			
					<img width="310"
					             height="240"
					             alt="Мы работаем! Идеальное сочетание от Cinema Towers 3D: 2 билета на любой сеанс + 2 маленьких попкорна + 2 колы и скидка 30%!"
					             src="../chocolife.me/static.chocolife.me/static/upload/images/deal/for_deal_page/40000/39421/310x240/8_2017110910112915102020090244.jpg"
					             class="e-deal__img"
					             >
								</div>



	<div class="b-deal__bought">
		<span class="e-deal__link">Купили:
	        <span class="e-deal__count" data-id="39421"> 1409 </span>
	    </span>
	</div>
	<div class="b-deal__info">
													<span class="e-deal__price e-deal__price--old ">
				3 200			</span>
								
									<!-- Рейтинг акции -->
					<div class=" otzivy_deal_raiting_element_rating" style="height: 26px">
						<div class="hart1_inbox_rating">
							<div class="hart_ico_rating"></div>
							<div class="deal_rating_rating"
								 style="width:101.6px">
							</div>
						</div>
					</div>
						<span class="e-deal__price ">
						2 240&nbsp;тг.		</span>
						<span class="e-link--deal e-link--deal-none-border">Подробнее</span>
	</div>
	<div class="e-deal__text-wrapper">
		<div class="e-deal__text-inner">
            <div class="e-deal__title e-deal__title-no-margin">
                Cinema Towers 3D            </div>
            <p class="e-deal__text" >
                Идеальное сочетание: 2 билета на любой сеанс + попкорн + 2 колы!            </p>
        </div>
	</div>
</a>
</li>
<div class="b-category__seotext" style="display: block;">
    <div class="b-category__seotext__pic_and_text">
                    <div class="b-category__seotext__pic">
                <img src="static/upload/images/deal/for_deal_categoryseo/1/1.jpg" alt="Все скидки и акции в одном месте! - Chocolife.me"
                     title="Все скидки и акции в одном месте!"/>
            </div>
                <div class="b-category__seotext__text">Покупайте купоны и&nbsp;экономьте на&nbsp;услугах и&nbsp;товарах!</h2> <p>В нашем купонном сервисе Шоколайф можно приобрести горящие скидки и купоны в Алматы с самым разным акционным диапазоном: от 30% до 90%. На&nbsp;сайте представлено более 530 разных возможностей для реализации личных планов. Например, вы&nbsp;можете в&nbsp;очередной раз посетить &laquo;Sarafan cafe&raquo;, но&nbsp;теперь со&nbsp;скидочным купоном на&nbsp;50% оставить там намного меньшую сумму. Такой купон обойдется вам всего в&nbsp;399&nbsp;тенге.</p> <p>Срок действия каждой акции в&nbsp;Алматы указан на&nbsp;нашем сайте. Эта информация поможет приобрести купон для посещения вами не&nbsp;только интересного предложения, но&nbsp;и&nbsp;оптимального во&nbsp;времени. Чтобы быстрее отыскать нужную акцию в нашем агрегаторе скидок, воспользуйтесь удобным поиском сайта онлайн по всем имеющимся рубрикам.</p> <p>Отныне и&nbsp;навсегда сайт акций и&nbsp;скидок в&nbsp;Алматы должен стать вашим лучшим другом и&nbsp;помощником! С&nbsp;нашей помощью вы&nbsp;сэкономите уйму денежных средств, невозвратимое время и&nbsp;дорогое здоровье. Удобное оформление сайта, правильно подобранная цветовая гамма, удобство в&nbsp;расположении рубрик не&nbsp;оставят равнодушным ни&nbsp;молодежь, ни&nbsp;людей преклонного возраста.</p> <p>Скидочные купоны в&nbsp;Алматы, представленные у&nbsp;нас&nbsp;&mdash; это гарантия настоящего качества. Мы&nbsp;ручаемся за&nbsp;каждого своего компаньона, уверены в&nbsp;безупречности каждого предложения, ведь все проверено лично нами! Если вы&nbsp;не&nbsp;успели применить скидку по&nbsp;назначению, или вас не&nbsp;устроило обслуживание&nbsp;&mdash; звоните по&nbsp;телефону, указанному на&nbsp;сайте, и&nbsp;наша служба заботы о&nbsp;пользователях обязательно даст вам все разъяснения. При возникновении вопросов или предложений также можно связаться с&nbsp;нами через форму обратной связи.</p> </div>
    </div>

    <div style="clear:both"></div>
</div>
<!-- seo text for categories -->



<footer>
	<div class="b-footer__wrapper">
		<div class="b-footer__top">
			<ul class="b-links__list">
				<li class="e-links__item e-links--head">Компания</li>
				<li class="e-links__item">
					<a href="info/about/index.html" title="О Chocolife.me" class="e-links__link">О Chocolife.me</a>
				</li>
				<li class="e-links__item">
					<a href="info/press/index.html" title="Пресса о нас" class="e-links__link">Пресса о нас</a>
				</li>
				<li class="e-links__item">
					<a href="info/contacts/index.html" title="Контакты" class="e-links__link">Контакты</a>
				</li>
			</ul>
			<ul class="b-links__list">
				<li class="e-links__item e-links--head">Клиентам</li>
				<li class="e-links__item">
					<a href="feedback/index.html" title="Обратная связь" class="e-links__link">Обратная связь</a>
				</li>
				<li class="e-links__item">
					<a href="info/tutorial/index.html" title="Обучающий видеоролик" class="e-links__link">Обучающий видеоролик</a>
				</li>
				<li class="e-links__item">
					<a href="faq/index.html" title="Вопросы и ответы" class="e-links__link">Вопросы и ответы</a>
				</li>
				<li class="e-links__item">
					<a href="info/terms/index.html" title="Публичная оферта" class="e-links__link">Публичная оферта</a>
				</li>
			</ul>
			<ul class="b-links__list">
				<li class="e-links__item e-links--head">Партнерам</li>
				<li class="e-links__item">
					<a href="partners/index.html" title="Для Вашего бизнеса" class="e-links__link">Для Вашего бизнеса</a>
				</li>
			</ul>
			<ul class="b-links__list" style="display:none;">
				<li class="e-links__item e-links--head">Алматы					<span class="e-icon js-e-current__city--main-down" style="cursor: pointer">&#203;</span>
				</li>
								<div class="e-telephone">+7 (727) 346-85-88</div>				
			</ul>

			<div class="b-links__list b-apps">
				<p class="e-links__item e-links--head e-apps__head">Наше приложение</p>

				<p class="e-apps__text">Chocolife.me теперь еще удобнее и<br /> всегда под рукой!</p>

				<div class="clear"></div>
				<a class="e-apps__android" rel="nofollow" href="https://play.google.com/store/apps/details?id=chocolife.me&amp;hl=ru"
				   target="_blank"
				   title="Мобильное приложение Chocolife.me для Android"></a>
				<a class="e-apps__ios" rel="nofollow" target="_blank" href="https://itunes.apple.com/kz/app/chocolife/id597833321"
				   title="Мобильное приложение Chocolife.me для iPhone"></a>
			</div>
		</div>
		<div class="b-footer__bottom">
			<span class="e-footer__choco">Chocolife.me | 2011-2018</span>
			<span class="e-footer__choco">&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;
				<a href="sitemap/index.html" title="Карта сайта">Карта сайта</a>
			</span>
			<a href="https://instagram.com/chocolife.me" rel="nofollow" class="e-icon e-icon--footer" title="instagram">
				<div class="e-icon--ins"></div>
			</a>
			<a href="https://www.youtube.com/user/chocolifeme" rel="nofollow" class="e-icon e-icon--footer" title="YouTube">
				<div class="e-icon--yt"></div>
			</a>
			<a href="https://www.facebook.com/chocolife.me" rel="nofollow" class="e-icon e-icon--footer" title="facebook">
				<div class="e-icon--fb"></div>
			</a>
			<a href="https://vk.com/chocofamily" rel="nofollow" class="e-icon e-icon--footer" title="vk">
				<div class="e-icon--vk"></div>
			</a>
			<span class="e-footer__social">Chocolife.me в социальных сетях:</span>
		</div>
	</div>
</footer>
</body>
</html>